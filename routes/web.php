<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//Pelajar(Pengundi)
//Route::get('/home', 'VoteController@index')->middleware('auth');
Route::get('/vote', 'VoteController@vote');
Route::post('/vote', 'VoteController@store');

//Admin
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::match(['get', 'post'], '/dashboard', 'AdminController@index');
    Route::match(['get', 'post'], '/candidate', 'AdminController@candidate');
    Route::match(['get', 'post'], '/candidate/create', 'AdminController@create_candidate');
    Route::match(['get', 'post'], '/role', 'AdminController@role');
    Route::match(['get', 'post'], '/role/create', 'AdminController@create_role');
    Route::match(['get', 'post'], '/role/{role}/edit', 'AdminController@edit_role');
    Route::match(['get', 'post'], '/votes', 'AdminController@vote');
});

Route::post('/role', 'AdminController@store_role');
Route::patch('/role/{role}', 'AdminController@update_role');
Route::delete('/role/{role}', 'AdminController@destroy_role');

Route::post('/candidate', 'AdminController@store_candidate');
