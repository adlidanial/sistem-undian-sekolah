<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Image;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /*public function __construct()
    {
        dd(auth()->user());
        if(auth()->user()->role != 'ADMIN')
        {
            $this->middleware('auth');
        }
    }*/

    /*public function admin(Request $req){
        return view('middleware')->withMessage('Admin');
    }*/

    // List of function Candidate
    public function index()
    {
        return view('admin.index');
    }

    public function candidate()
    {
        $results = DB::select('select a.name, a.matric, b.name as role, c.filename
        from users a, roles b, images c, candidates d
        where d.role_id = b.id and d.user_id = a.id and c.candidate_id = d.id');
        //$candidates = Candidate::all();
        return view('admin.candidate', compact('results'));
    }

    public function create_candidate()
    {
        $roles = Role::where('status', 'Y')->get();
        return view('admin.candidate.index', compact('roles'));
    }

    public function store_candidate()
    {
        $data = request()->validate([
            'name' => 'required',
            'matric' => 'required',
            'role' => 'required|nullable',
            'image' => 'required',
        ]);

        $users = DB::select('select * from users where name = :name and matric = :matric',[
            'name' => $data['name'],
            'matric' => $data['matric']
        ]);

        $roles = DB::select('select * from roles where id = :name',[
            'name' => $data['role'],
        ]);
        //dd($users);
        foreach ($users as $user)
        {
            foreach($roles as $role)
            {
                if($user->name != '' && $user->matric != '')
                {
                    $table = new Candidate();
                    $table->user_id = $user->id;
                    $table->role_id = $role->id;
                    $table->save();
                }
            }
        }

        $candidates = DB::select('select * from candidates order by id desc limit 1');
        //dd($roles);
        $fileName = time().'.'.$data['image']->getClientOriginalExtension();
        $data['image']->move(public_path('uploads'), $fileName);
        foreach ($candidates as $candidate)
        {
            $table = new Image();

            $table->candidate_id = $candidate->id;
            $table->filename = $fileName;
            $table->save();
            return redirect('/candidate')->with('success','Tambah calon berjaya!');
        }

        return redirect('/candidate/create')->with('error','Nama calon ini bukan merupakan pelajar di sekolah ini!');
    }


    // List of function Role

    public function role()
    {
        $roles = Role::all();
        return view('admin.role', compact('roles'));
    }

    public function create_role()
    {
        return view('admin.role.index');
    }

    public function store_role()
    {
        $data = request()->validate([
            'name' => 'required',
            'status' => 'required|nullable',
        ]);

        $table = new Role();
        $table->name = $data['name'];
        $table->status = $data['status'];
        $table->save();

        return redirect('/role')->with('success','Tambah peranan berjaya!');
    }

    public function edit_role(Role $role)
    {
        return view('admin.role.edit', compact('role'));
    }

    public function update_role(Role $role)
    {
        $data = request()->validate([
            'name' => 'required',
            'status' => 'required|nullable',
        ]);

        $role->update($data);

        return redirect('/role')->with('success','Kemaskini peranan berjaya!');
    }

    public function destroy_role(Role $role)
    {
        $role->delete();

        return redirect('/role')->with('success','Hapus peranan berjaya!');
    }


    public function vote()
    {
        $results = DB::select('SELECT
                A.candidate_id,
                COUNT(A.candidate_id) AS countvote,
                B.user_id,
                B.role_id,
                C.name,
                C.matric,
                D.name AS role
                FROM votes A, candidates B, users C, roles D
                WHERE A.candidate_id = B.id AND B.user_id = C.id AND B.role_id = D.id
                GROUP BY B.user_id, A.candidate_id, B.role_id, C.name, C.matric, D.name');

        return view('admin.vote', compact('results'));
    }
}
