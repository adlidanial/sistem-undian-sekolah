<?php

namespace App\Http\Controllers;

use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('voting.index');
    }

    public function vote()
    {
        $results = DB::select('select a.name as nameuser, a.matric, b.name as role, c.filename, d.id
        from users a, roles b, images c, candidates d
        where d.role_id = b.id and d.user_id = a.id and c.candidate_id = d.id');
        //dd($results);
        return view('voting.vote', compact('results'));
    }

    public function store()
    {
        $data = request()->validate([
            'candidate_id' => 'required',
        ]);

        $table = new Vote();
        $table->candidate_id = $data['candidate_id'];
        $table->save();

        return redirect('/home')->with('success','Undian anda telah disimpan. Terima kasih.');

    }

    public function success()
    {

    }
}
