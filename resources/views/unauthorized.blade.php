@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Warning!</div>
                    <div class="card-body">
                        You cannot access this page! Return <a href="/">Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

