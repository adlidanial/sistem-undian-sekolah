@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Senarai Calon</div>
                    <div class="card-body">
                        <div class="form-group">
                            <a href="/role" class="btn btn-primary">Kembali</a>
                        </div>
                        <form action="/role" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nama Peranan</label>
                                <input type="text" class="form-control" id="name" placeholder="Nama Peranan" aria-describedby="NameHelp" name="name">
                                @error('name')
                                <p class="text-danger">Sila masukkan nama peranan.</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="">Sila Pilih</option>
                                    <option value="Y">Ya</option>
                                    <option value="N">Tidak</option>
                                </select>
                                @error('status')
                                <p class="text-danger">Sila pilih status.</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
