@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Senarai Peranan</div>
                    <div class="card-body">
                        <div class="form-group">
                            <a href="/role/create" class="btn btn-primary">Tambah Peranan</a>
                        </div>

                        <hr>
                        <table class="table">
                            <thead class="thead-dark">

                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Nama Peranan</th>
                                <th scope="col">Status</th>
                                <th scope="col">Tarikh Kemaskini</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($roles as $role)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{$role->name}}</td>
                                    <td>{{$role->status}}</td>
                                    <td>{{$role->updated_at}}</td>
                                    <td><a href="/role/{{$role->id}}/edit" class="btn btn-primary">Kemaskini</a></td>
                                    <td>
                                        <form action="/role/{{$role->id}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                        </form>
                                    </td>
                                </tr>

                            @empty

                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
