@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Senarai Calon</div>
                    <div class="card-body">
                        <div class="form-group">
                            <a href="/candidate" class="btn btn-primary">Kembali</a>
                        </div>
                        <form action="/candidate" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nama Calon</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Nama Calon" aria-describedby="nameHelp">
                            </div>
                            @error('name')
                            <p class="text-danger">Sila masukkan nama calon.</p>
                            @enderror
                            <div class="form-group">
                                <label for="matric">No. Kad Pelajar</label>
                                <input type="text" name="matric" class="form-control" placeholder="No. Kad Pelajar" id="matric">
                            </div>
                            @error('name')
                            <p class="text-danger">Sila masukkan no kad pelajar.</p>
                            @enderror
                            <div class="form-group">
                                <label for="role">Peranan</label>
                                <select class="form-control" id="role" name="role">
                                    <option>Sila Pilih</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role')
                                <p class="text-danger">Sila pilih peranan.</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Gambar</label><br>
                                <input type="file" name="image" id="image">
                                @error('name')
                                <p class="text-danger">Sila masukkan gambar calon.</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
