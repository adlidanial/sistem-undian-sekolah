@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Senarai Undian</div>
                    <div class="card-body">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Nama Pelajar</th>
                                <th scope="col">No. Kad Pelajar</th>
                                <th scope="col">Peranan</th>
                                <th scope="col">Bilangan Undian</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($results as $result)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{$result->name}}</td>
                                    <td>{{$result->matric}}</td>
                                    <td>{{$result->role}}</td>
                                    <td>{{$result->countvote}}</td>
                                </tr>

                            @empty

                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
