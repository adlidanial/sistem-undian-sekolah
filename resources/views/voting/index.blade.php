@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Perhatian!</div>
                    <div class="card-body">
                        <p>1. Pelajar perlu pilih salah satu calon mengikut peranan calon.</p>
                        <p>2. Selamat mengundi.</p>

                        <div class="form-group">
                            <a href="/vote" class="btn btn-primary float-right">Teruskan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
