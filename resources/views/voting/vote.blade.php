@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Senarai Calon</div>
                    <div class="card-body">
                        <form action="/vote" method="post">
                            @csrf
                            <div class="row">
                                @forelse($results as $result)
                                <div class="col-md-6 col-lg-4 col-12 text-center">
                                    <input type="radio" value="{{$result->id}}" name="candidate_id" id=""><br>
                                    <img src="/uploads/{{$result->filename}}" alt="" height="200" width="180">
                                    <p>{{$result->nameuser}}</p>
                                    <p>Sebagai : {{$result->role}}</p>
                                </div>

                                @empty
                                @endforelse
                            </div>
                            <div class="col-md-12 col-lg-12 col-12 text-center">
                                <button type="submit" class="btn btn-primary">Undi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
